<?php 
/**
*  Corresponding Class to test DataGenerator class
*
 * @author jgarcia@ayco.net
*/

class DataGeneratorTest extends PHPUnit_Framework_TestCase{
	
  /**
  * Just check if the YourClass has no syntax error 
  *
  * This is just a simple check to make sure your library has no syntax error. This helps you troubleshoot
  * any typo before you even use this library in a real project.
  *
  */
  public function testIsThereAnySyntaxError(){
	$var = new Ayco\DataGenerator;
	$this->assertTrue(is_object($var));
	unset($var);
  }
  
  /**
  * Just check if the YourClass has no syntax error 
  *
  * This is just a simple check to make sure your library has no syntax error. This helps you troubleshoot
  * any typo before you even use this library in a real project.
  *
  */
  public function testVersion(){
	$var = new Ayco\DataGenerator;
	$this->assertTrue($var->getVersion() == $var::VERSION);
	unset($var);
  }
  
}