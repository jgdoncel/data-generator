<?php
/**
 * Carga de fichero de configuracion
 *
 * @author jgarcia@ayco.net
 * @date 04/04/2018 11:03
 */

namespace Ayco\DataGenerator;


class Config
{

    private $host;
    private $dbname;
    private $user;
    private $pass;
    private $port;
    private $path_to_lib;
    private $namespace;
    private $views;
    private $tables;
    private $procedures;
    private $dump;
    private $clean;

    /**
     * Config constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct($config)
    {
        $this->setHost(isset($config->host) ? $config->host : null);
        $this->setDbname(isset($config->dbname) ? $config->dbname : null);
        $this->setUser(isset($config->user) ? $config->user : null);
        $this->setPass(isset($config->pass) ? $config->pass : null);
        $this->setPort(isset($config->port) ? $config->port : null);
        $this->setPathToLib(isset($config->path_to_lib) ? $config->path_to_lib : null);
        $this->setNamespace(isset($config->namespace) ? $config->namespace : null);
        $this->setViews(isset($config->objects->views) ? $config->objects->views : null);
        $this->setTables(isset($config->objects->tables) ? $config->objects->tables : null);
        $this->setProcedures(isset($config->objects->procedures) ? $config->objects->procedures : null);
        $this->setDump(isset($config->options->dump) ? $config->options->dump : null);
        $this->setClean(isset($config->options->clean) ? $config->options->clean : null);
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @throws \Exception
     */
    public function setHost($host)
    {
        $this->host = $host;
        if (!isset($this->host)) throw new \Exception('No se ha encontrado HOST');
    }

    /**
     * @return mixed
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * @param mixed $dbname
     * @throws \Exception
     */
    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
        if (!isset($this->dbname)) throw new \Exception('No se ha encontrado DBNAME');
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @throws \Exception
     */
    public function setUser($user)
    {
        $this->user = $user;
        if (!isset($this->user)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado USER');
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     * @throws \Exception
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
        if (!isset($this->pass)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado PASS');
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     * @throws \Exception
     */
    public function setPort($port)
    {
        $this->port = $port;
        if (!isset($this->port)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado PORT');
    }

    /**
     * @return mixed
     */
    public function getPathToLib()
    {
        return $this->path_to_lib;
    }

    /**
     * @param mixed $path_to_lib
     * @throws \Exception
     */
    public function setPathToLib($path_to_lib)
    {
        $this->path_to_lib = $path_to_lib;
        if (!isset($this->path_to_lib)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado PATH_TO_LIB');
    }

    /**
     * @return mixed
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param mixed $namespace
     * @throws \Exception
     */
    public function setNamespace($namespace)
    {
        $this->namespace = str_replace("\\", "/", $namespace);
        if (!isset($this->namespace)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado NAMESPACE');
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     * @throws \Exception
     */
    public function setViews($views)
    {
        $this->views = $views;
        if (!isset($this->views)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado VIEWS (objects.views)');
    }

    /**
     * @return mixed
     */
    public function getTables()
    {
        return $this->tables;
    }

    /**
     * @param mixed $tables
     * @throws \Exception
     */
    public function setTables($tables)
    {
        $this->tables = $tables;
        if (!isset($this->tables)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado TABLES (objects.tables)');
    }

    /**
     * @return mixed
     */
    public function getProcedures()
    {
        return $this->procedures;
    }

    /**
     * @param mixed $procedures
     * @throws \Exception
     */
    public function setProcedures($procedures)
    {
        $this->procedures = $procedures;
        if (!isset($this->procedures)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado PROCEDURES (objects.procedures)');
    }

    /**
     * @return mixed
     */
    public function getDump()
    {
        return $this->dump;
    }

    /**
     * @param mixed $dump
     * @throws \Exception
     */
    public function setDump($dump)
    {
        $this->dump = $dump;
        if (!isset($this->dump)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado DUMP (options.dump)');
    }

    /**
     * @return mixed
     */
    public function getClean()
    {
        return $this->clean;
    }

    /**
     * @param mixed $clean
     * @throws \Exception
     */
    public function setClean($clean)
    {
        $this->clean = $clean;
        if (!isset($this->clean)) throw new \Exception($this->host . '/' . $this->dbname . ' - No se ha encontrado CLEAN (options.clean)');
    }


}