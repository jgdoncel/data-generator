<?php
/**
 *  Ayco\DataGenerator
 *
 *  Genera clases de acceso DAO
 *
 * @author jgarcia@ayco.net
 * @version 2.0.0
 * @date 2018-03-07
 */

namespace Ayco;

use Ayco\DataGenerator\Config;
use PDO;
use PDOException;
use Twig_Environment;
use Twig_Loader_Filesystem;

class DataGenerator
{

    /**
     * @var string Versión de la librería
     */
    private $version = '2.0.0';

    /**
     * @var DataGenerator\Config Configuración
     */
    private $config;

    /**
     * @var \PDO MySQL PDO Handler
     */
    private $dbhandler;

    private $arOptions;

    public $tables;
    public $views;
    public $columns;
    public $procedures;
    public $parameters;
    public $pks;

    /**
     * DataGenerator constructor.
     * @param null $arOptions
     */
    public function __construct($arOptions = null)
    {
        $this->arOptions = $arOptions;
    }

    /**
     * Ejecuta la generación de código
     * @param $pathToConfigFile string Ruta al fichero de configuración
     * @return bool
     * @throws \Exception
     */
    public function execute($pathToConfigFile = null)
    {
        if (!isset($pathToConfigFile)) $pathToConfigFile = '.data-generator.json';

        $config = $this->loadConfigFile($pathToConfigFile);

        for ($i = 0; $i < count($config); $i++) {

            $this->config = new Config($config[$i]);

            if ($this->config === null) {
                throw new \Exception("Error cargando el archivo de configuracion: " . $this->showJsonError());
            } else {
                $this->manageDirectories();
                $this->getStructure();
                $this->process();
                $this->writeLockFile();
            }

        }

        return true;
    }

    /**
     * Carga el fichero de configuración
     * @param $pathToConfigFile
     * @return mixed
     * @throws \Exception
     */
    private function loadConfigFile($pathToConfigFile)
    {
        if (is_file($pathToConfigFile)) {
            if (isset($this->arOptions['--verbose'])) $this->output("Cargando el fichero " . $pathToConfigFile);
            $dataString = file_get_contents($pathToConfigFile);
            if ($dataString == '') {
                throw new \Exception("El fichero de configuracion no es legible o esta vacio");
            }
            return json_decode($dataString);
        } else {
            throw new \Exception("No se ha encontrado el fichero " . $pathToConfigFile);
        }
    }

    /**
     * Carga el fichero de bloqueo
     * @param $pathToLockfile
     * @return mixed
     * @throws \Exception
     */
    private function loadLockFile($pathToLockfile)
    {
        if (!is_file($pathToLockfile))  $this->writeLockFile($pathToLockfile);
        if (is_file($pathToLockfile)) {
            if (isset($this->arOptions['--verbose'])) $this->output("Cargando el fichero " . $pathToLockfile);
            $dataString = file_get_contents($pathToLockfile);
            if ($dataString == '') {
                throw new \Exception("El fichero de bloqueo no es legible o esta vacio");
            }
            return json_decode($dataString, true);
        } else {
            throw new \Exception("No se ha encontrado el fichero " . $pathToLockfile);
        }
    }

    /**
     * @throws \Exception
     */
    private function manageDirectories()
    {
        // Verificar que exista el directorio raíz
        if (!is_dir('./' . $this->config->getPathToLib() . '/' . $this->config->getNamespace())) throw new \Exception("No existe el directorio " . $this->config->getPathToLib() . '/' . $this->config->getNamespace());
        // Crear directorios necesarios
        $this->createDirectory('Core');
        $this->createDirectory('DAO');
        $this->createDirectory('DTO');
        $this->createDirectory('SP');
        $this->createDirectory('Utils');
    }

    /**
     * Crea un directorio
     * @param $dir
     * @throws \Exception
     */
    private function createDirectory($dir)
    {
        $fullDir = $this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/' . $dir;
        if (!is_dir($fullDir)) {
            if (!mkdir($fullDir)) throw new \Exception('No se puede crear el directorio ' . $fullDir);
        }
    }

    /**
     * Lee la estructura de datos
     * @throws \Exception
     */
    private function getStructure()
    {
        try {
            $this->dbhandler = new PDO(
                'mysql:dbname=' . $this->config->getDbname() . ';host=' . $this->config->getHost(),
                $this->config->getUser(),
                $this->config->getPass(),
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"
            ));
            $this->tables = $this->dbhandler->query("select TABLE_NAME, TABLE_COMMENT from INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . $this->config->getDbname() . "' AND LEFT(TABLE_NAME,1)<>'_' AND TABLE_TYPE <> 'VIEW'")->fetchAll(PDO::FETCH_ASSOC);
            $this->views = $this->dbhandler->query("select TABLE_NAME from INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '" . $this->config->getDbname() . "' AND LEFT(TABLE_NAME,1)<>'_'")->fetchAll(PDO::FETCH_ASSOC);
            $this->columns = $this->dbhandler->query("select TABLE_NAME, COLUMN_NAME, COLUMN_DEFAULT, COLUMN_COMMENT, DATA_TYPE, COLUMN_TYPE, COLUMN_KEY, EXTRA from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . $this->config->getDbname() . "' AND LEFT(TABLE_NAME,1)<>'_' ORDER BY TABLE_NAME, ORDINAL_POSITION")->fetchAll(PDO::FETCH_ASSOC);
            $this->buildVarTypes();
            $this->pks = $this->dbhandler->query("select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_SCHEMA = '" . $this->config->getDbname() . "'")->fetchAll(PDO::FETCH_ASSOC);
            $this->procedures = $this->dbhandler->query("select ROUTINE_NAME, ROUTINE_COMMENT from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = '" . $this->config->getDbname() . "' AND ROUTINE_TYPE = 'PROCEDURE'")->fetchAll(PDO::FETCH_ASSOC);
            $this->parameters = $this->dbhandler->query("select SPECIFIC_NAME, PARAMETER_NAME, PARAMETER_MODE, DATA_TYPE, DTD_IDENTIFIER, SPECIFIC_NAME from INFORMATION_SCHEMA.PARAMETERS WHERE SPECIFIC_SCHEMA = '" . $this->config->getDbname() . "' AND ROUTINE_TYPE = 'PROCEDURE' AND PARAMETER_MODE = 'IN'")->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function process()
    {
        $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'templates';
        $loader = new Twig_Loader_Filesystem($dir);
        $twig = new Twig_Environment($loader);

        $twig_values = array(
            'namespace' => str_replace("/", "\\", $this->config->getNamespace()),
            'version' => $this->getVersion(),
            'author' => 'AYCO INTERNET S.L.'
        );

        if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/Core/Database.php', 'w')) {
            fwrite($fp, $twig->render('database.twig', $twig_values));
            fclose($fp);
        }

        if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/Utils/QueryAdapter.php', 'w')) {
            fwrite($fp, $twig->render('query_adapter.twig', $twig_values));
            fclose($fp);
        }

        
        if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/Utils/QueryAdapterDatatables.php', 'w')) {
            fwrite($fp, $twig->render('query_adapter_datatables.twig', $twig_values));
            fclose($fp);
        }

        if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/Utils/ResultAdapter.php', 'w')) {
            fwrite($fp, $twig->render('result_adapter.twig', $twig_values));
            fclose($fp);
        }

        foreach ($this->tables as $table) {

            $className = str_replace(' ', '', ucwords(str_replace('_', ' ', $table['TABLE_NAME'])));
            $twig_values['className'] = $className;
            $twig_values['table'] = $table;
            $twig_values['tableName'] = $table['TABLE_NAME'];
            $twig_values['tableComment'] = $table['TABLE_COMMENT'];
            $twig_values['columns'] = array_filter($this->columns, function ($value) use ($table) {
                return $value['TABLE_NAME'] == $table['TABLE_NAME'] && $value['DATA_TYPE'] != 'timestamp';
            });
            $twig_values['pks'] = array_filter($this->columns, function ($value) use ($table) {
                return $value['TABLE_NAME'] == $table['TABLE_NAME'] && $value['COLUMN_KEY'] == 'PRI';
            });
            $twig_values['insert'] = array_filter($this->columns, function ($value) use ($table) {
                return $value['TABLE_NAME'] == $table['TABLE_NAME'] && $value['EXTRA'] != 'auto_increment' && $value['DATA_TYPE'] != 'timestamp';
            });
            $twig_values['update'] = array_filter($this->columns, function ($value) use ($table) {
                return $value['TABLE_NAME'] == $table['TABLE_NAME'] && $value['COLUMN_KEY'] != 'PRI' && $value['DATA_TYPE'] != 'timestamp';
            });
            if (!file_exists($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/' . $className . '.php')) {
                if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/' . $className . '.php', 'w')) {
                    fwrite($fp, $twig->render('controller.twig', $twig_values));
                    fclose($fp);
                }
            }
            if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/DAO/' . $className . '.php', 'w')) {
                fwrite($fp, $twig->render('dao_table.twig', $twig_values));
                fclose($fp);
            }
            if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/DTO/' . $className . '.php', 'w')) {
                fwrite($fp, $twig->render('dto.twig', $twig_values));
                fclose($fp);
            }

        }

        foreach ($this->views as $view) {
            $className = str_replace(' ', '', ucwords(str_replace('_', ' ', $view['TABLE_NAME'])));
            $twig_values['className'] = $className;
            $twig_values['viewName'] = $view['TABLE_NAME'];
            $twig_values['columns'] = array_filter($this->columns, function ($value) use ($table) {
                return $value['TABLE_NAME'] == $table['TABLE_NAME'] && $value['DATA_TYPE'] != 'timestamp';
            });
            if (!file_exists($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/' . $className . '.php')) {
                if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/' . $className . '.php', 'w')) {
                    fwrite($fp, $twig->render('controller.twig', $twig_values));
                    fclose($fp);
                }
            }
            if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/DAO/' . $className . '.php', 'w')) {
                fwrite($fp, $twig->render('dao_view.twig', $twig_values));
                fclose($fp);
            }
            if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/DTO/' . $className . '.php', 'w')) {
                fwrite($fp, $twig->render('dto.twig', $twig_values));
                fclose($fp);
            }
        }

        foreach ($this->procedures as $procedure) {
            $className = str_replace(' ', '', ucwords(str_replace('_', ' ', $procedure['ROUTINE_NAME'])));
            $twig_values['className'] = $className;
            $twig_values['procedureName'] = $procedure['ROUTINE_NAME'];
            $twig_values['procedureComment'] = $procedure['ROUTINE_COMMENT'];
            $twig_values['parameters'] = array_filter($this->parameters, function ($value) use ($procedure) {
                return $value['SPECIFIC_NAME'] == $procedure['ROUTINE_NAME'];
            });
            if ($fp = fopen($this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/SP/' . $className . '.php', 'w')) {
                fwrite($fp, $twig->render('procedure.twig', $twig_values));
                fclose($fp);
            }
        }
        $this->clean();

    }

    /**
     * Limpia ficheros obsoletos en la carpeta Data
     */
    private function clean() {
        if ($this->config->getClean()&&false) {
            // Limpiar DTO
            $directory = $this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/DTO/';
            $files = glob($directory . "*.*");
            foreach($files as $file) {
                if (!in_array($file, $this->objects['DTO'])) {
                    $this->output(@unlink($file) ? "Eliminado DTO: " . $file : "Error al eliminar DTO: " . $file, true);
                }
            }
            // Limpiar DAO
            $directory = $this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/DAO/';
            $files = glob($directory . "*.*");
            foreach($files as $file) {
                if (!in_array($file,  $this->objects['DAO'])) {
                    $this->output(@unlink($file) ? "Eliminado DAO: " . $file : "Error al eliminar DAO: " . $file, true);
                }
            }
            // Limpiar base
            $directory = $this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/';
            $files = glob($directory . "*.*");
            foreach($files as $file) {
                if (!in_array($file,  $this->objects['DATA'])) {
                    $this->output(@unlink($file) ? "Eliminado Controller: " . $file : "Error al eliminar Controller: " . $file, true);
                }
            }
            // Limpiar SP
            $directory = $this->config->getPathToLib() . '/' . $this->config->getNamespace() . '/SP/';
            $files = glob($directory . "*.*");
            foreach($files as $file) {
                if (!in_array($file,  $this->objects['SP'])) {
                    $this->output(@unlink($file) ? "Eliminado SP: " . $file : "Error al eliminar SP: " . $file, true);
                }
            }
        }
    }

    private function buildVarTypes()
    {
        for ($i = 0; $i < count($this->columns); $i++) {
            switch ($this->columns[$i]['DATA_TYPE']) {
                case 'varchar':
                case 'text':
                    $this->columns[$i]['VAR_TYPE'] = 'string';
                    break;
                case 'int':
                case 'bigint':
                    $this->columns[$i]['VAR_TYPE'] = 'int';
                    break;
                case 'float':
                case 'decimal':
                    $this->columns[$i]['VAR_TYPE'] = 'float';
                    break;
                case 'double':
                    $this->columns[$i]['VAR_TYPE'] = 'double';
                    break;
                case 'tinyint':
                    if ($this->columns[$i]["COLUMN_TYPE"] == 'tinyint(1)') {
                        $this->columns[$i]['VAR_TYPE'] = 'bool';
                    } else {
                        $this->columns[$i]['VAR_TYPE'] = 'int';
                    }
                    break;
                case 'datetime':
                    $this->columns[$i]['VAR_TYPE'] = 'string';
                    break;
                case 'date':
                    $this->columns[$i]['VAR_TYPE'] = 'string';
                    break;
                case 'time':
                    $this->columns[$i]['VAR_TYPE'] = 'string';
                    break;
                default:
                    $this->columns[$i]['VAR_TYPE'] = $this->columns[$i]["DATA_TYPE"];
                    break;
            }
        }
    }

    /**
     * Obtiene información sobre el estado de actualización
     * @param null $pathToConfigFile
     * @return bool
     * @throws \Exception
     */
    public function status($pathToConfigFile = null)
    {
        if (!isset($pathToConfigFile)) $pathToConfigFile = '.data-generator.json';
        $config = $this->loadConfigFile($pathToConfigFile);
        for ($i = 0; $i < count($config); $i++) {
            $this->config = new Config($config[$i]);
            if ($this->config === null) {
                throw new \Exception("Error cargando el archivo de configuracion: " . $this->showJsonError());
            } else {
                $this->check();
            }
        }
        return true;
    }

    /**
     * @param null $pathToLockFile
     * @throws \Exception
     */
    private function check($pathToLockFile = null)
    {
        if (!isset($pathToLockFile)) $pathToLockFile = '.data-generator.lock';
        $lockInfo = $this->getLockInfo();
        $lockFile = $this->loadLockFile($pathToLockFile);
        $warning = false;
        if ($lockFile['mysql_version'] != $lockInfo['mysql_version']) {
            $this->outputError("La versión de MySQL ha cambiado a {$lockInfo['mysql_version']} (versión previa {$lockFile['mysql_version']})");
            $warning = true;
        }
        if ($lockFile['last_table_update'] != $lockInfo['last_table_update']) {
            $this->outputError("Las tablas se han actualizado es necesario regenerar las clases (actualizado {$lockInfo['last_table_update']})");
            $warning = true;
        }
        $diffViews = array_diff_assoc($lockFile['views'], $lockInfo['views']);
        $diffProcedures = array_diff_assoc($lockFile['procedures'], $lockInfo['procedures']);
        if (count($diffViews)>0) {
            foreach ($diffViews as $key => $value) {
                $this->outputError("[{$key}] Vista desactualizada o inexistente");
                $warning = true;
        }
        }
        if (count($diffProcedures)>0) {
            foreach ($diffProcedures as $key => $value) {
                $this->outputError("[{$key}] SP desactualizado o inexistente");
                $warning = true;
            }
        }
        
        if (isset($this->arOptions['--status']) && $warning) {
            $this->outputError("Ejecute la herramienta data-generator");
        } elseif (isset($this->arOptions['--status'])) {
            $this->output("Las clases de acceso a datos estan actualizadas");
        }


    }

    /**
     * Devuelve el número de versión
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Devuelve la configuración
     * @return null
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Show the json parse error that happened last
     * @return string Información del error de procesamiento
     */
    private function showJsonError()
    {
        $constants = get_defined_constants(true);
        $json_errors = array();
        foreach ($constants['json'] as $name => $value) {
            if (!strncmp($name, 'JSON_ERROR_', 11)) {
                $json_errors[$value] = $name;
            }
        }
        return $json_errors[json_last_error()];
    }

    /**
     * Send a string to the output stream, but only if --quiet is not enabled
     *
     * @param $str string A string output
     * @param bool $newLine
     */
    private function output($str, $newLine = true)
    {
        if (!isset($this->arOptions['--quiet'])) {
            echo($newLine ? $str . "\n" : $str);
        }
    }


    private function outputError($str)
    {
        if ($this->isCommandLineInterface()) {
            echo "\033[31m" . $str . "\033[0m\n";
        } else {
            echo $str . "\n";
        }
    }

    /**
     * Writes a lock file
     * @param null $pathToLockFile
     * @throws \Exception
     */
    private function writeLockFile($pathToLockFile = null)
    {
        if (!isset($pathToLockFile)) $pathToLockFile = '.data-generator.lock';
        if (DataGenerator::generateLockFile($pathToLockFile)) {
            $lockFile = fopen($pathToLockFile, "w");
            fwrite($lockFile, json_encode($this->getLockInfo()));
            fclose($lockFile);
        } else {
            throw new \Exception('No se puede generar el fichero lock');
        }
    }

    /**
     * @param null $pathToLockFile
     * @return bool
     */
    private static function generateLockFile($pathToLockFile)
    {
        if (!is_file($pathToLockFile)) {
            $lockFile = fopen($pathToLockFile, "w");
            fclose($lockFile);
        }
        return is_file($pathToLockFile);
    }

    /**
     * Lee la estructura de datos
     * @throws \Exception
     */
    private function getLockInfo()
    {
        try {
            if (!$this->dbhandler) {
                $this->dbhandler = new PDO(
                    'mysql:dbname=' . $this->config->getDbname() . ';host=' . $this->config->getHost(),
                    $this->config->getUser(),
                    $this->config->getPass(),
                    array(
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"
                    ));
            }
            $version = $this->dbhandler->query("SELECT VERSION() as version, NOW() AS ts;")->fetch(PDO::FETCH_ASSOC);
            $last_updated_table = $this->dbhandler->query("SELECT CREATE_TIME from INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . $this->config->getDbname() . "' AND LEFT(TABLE_NAME,1)<>'_' AND TABLE_TYPE <> 'VIEW' ORDER BY CREATE_TIME DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
            $_views = $this->dbhandler->query("SELECT TABLE_NAME AS name, MD5(VIEW_DEFINITION) AS hash FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '" . $this->config->getDbname() . "'")->fetchAll(PDO::FETCH_ASSOC);
            $_procedures = $this->dbhandler->query("SELECT SPECIFIC_NAME AS name, MD5(ROUTINE_DEFINITION) AS hash FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = '" . $this->config->getDbname() . "'")->fetchAll(PDO::FETCH_ASSOC);
            $views = array();
            foreach ($_views as $view) {
                $views[$view['name']] = $view['hash'];
            }
            $procedures = array();
            foreach ($_procedures as $procedure) {
                $procedures[$procedure['name']] = $procedure['hash'];
            }
            $return = array(
                'ts' => $version['ts'],
                'mysql_version' => $version['version'],
                'last_table_update' => $last_updated_table['CREATE_TIME'],
                'views' => $views,
                'procedures' => $procedures
            );
            return $return;
        } catch (PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }


    /**
     * Check the execution environment
     * @return bool True = Console / False = Web Server
     */
    private function isCommandLineInterface()
    {
        return (php_sapi_name() === 'cli');
    }

}
